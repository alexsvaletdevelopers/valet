<?php

/**
 * 
 * @category   CategoryName
 * @package    PackageName
 * @author     Alex Schittko <alex@bracketangles.net>
 * @copyright  2017 Alex Schittko
 * @license    Private
 */

/**
 * Description of logger
 *
 * @author alexschittko
 */
class logger {
    function __construct() {
        $this->config = new config();
    }
    /**
     * @title log($string)
     * @desc Create a a log entry with string
     * 
     * @param type $key Log key (user, checkin, checkout)
     * @param type $string Log entry
     * 
     * @return bool True on success, false on failure
     */
    function log($key, $string) {
        $query = "INSERT INTO log (`key`, event, user, timestamp) VALUES ('".$key."', '".$string."', '".$_SESSION['uid']."', '".time()."')";
        if (!$this->config->mysqlQuery($query, false)) {
            //die("failed to write log");
            return false;
        }
        else
            return true;
    }
    
    function displayAll() {
        $query = "SELECT * FROM log";
        $result = $this->config->mysqlQuery($query);
        if (is_array($result)) {
            return $result;
        }
        else {
            return false;
        }
        
    }
    
}
