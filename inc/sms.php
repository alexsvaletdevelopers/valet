<?php


/**
 * 
 *
 *
 * @category   CategoryName
 * @package    PackageName
 * @author     Alex Schittko <alex@bracketangles.net>
 * @copyright  2017 Alex Schittko
 * @license    Private
 */

/**
 * Description of sms
 *
 * @author alexschittko
 */
require('smsGateway.php');
require_once('config.php');
class sms {
    function __construct() {
        $this->config = new config();
        $this->deviceID = 68162;
        $this->gateway = new smsGateway($this->config->smsUser, $this->config->smsPass);
        $this->msg = '';
    }
    public function sms($slug, $number, $keyNumber = "") {
        switch($slug) {
            case 'checkin': $data = array('keyNumber' => $keyNumber); $this->msg = $this->config->checkinMessage($data); break;
            case 'checkout': $this->msg = $this->config->checkoutMessage(); break;
            case 'closing': $this->msg = $this->config->closingMessage(); break;
            default: $this->msg = $slug; break;
        }
        return $this->send($number);
    }
    private function send($number) {
        return $this->gateway->sendMessageToNumber($number, $this->msg, $this->deviceID);
    }
    public function getDeviceID() {
        return $this->deviceID;
    }
}

//$sms = new sms();
//print_r($sms->sms('checkin', '9728548298', uniqid()));
//print_r($sms->gateway->sendMessageToNumber('9728548298', uniqid(), $sms->getDeviceID));
