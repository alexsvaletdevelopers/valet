<?php

/**
 * 
 * HTML header for theme file
 * 
 * @author     Alex Schittko <alex@bracketangles.net>
 * @copyright  2017 Alex Schittko
 * @license    Private
 */
$config = new config();
?>
  
<html>
    <head>
        <title>Valet.</title>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js" type="text/javascript"></script>
 <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
    <script>
    function showUser(str)
    {
    if (str=="")
    {
        document.getElementById("phone").value="";
        return;
    }
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            var data = JSON.parse(xmlhttp.responseText);
            for(var i=0;i<data.length;i++) 
            {
              document.getElementById("name").value = data[i].name;
            }
        }
    }
    xmlhttp.open("GET","ajax.php?f=gc&q="+str,true);
    xmlhttp.send();
    }
    </script>
   <script>
    function updateLocation(str, keyid)
    {
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.open("GET","ajax.php?f=ul&q="+str+"&k="+keyid,true);
    xmlhttp.send();
    }
    </script>
 
    <style>
        #nav { font-size: 6vw; }
        a {font-size: 4vw;}
        tr, td { text-align: center; }
        .left {align-content: left; }
        .center {align-content: center; }
        .right {align-content: right; }
        .input {height: 8vw; }
    </style>
    <body>
        <p align="center"><a href="/index.php" name="nav">Home</a> | <a href="/index.php?action=keyboard">Key Board</a> | <a href="/index.php?action=checkin">Check-in</a> | <a href="/index.php?action=viewLog">Log</a></p>