<?php

/**
 * 
 *
 *
 * @category   CategoryName
 * @package    PackageName
 * @author     Alex Schittko <alex@bracketangles.net>
 * @copyright  2017 Alex Schittko
 * @license    Private
 */

/**
 * Description of user
 *
 * @author alexschittko
 */
class user {
    function __construct() {
        $this->config = new config();
        $this->logger = new logger();
    }
    /**
     * @title get($field, $uid)
     * @desc Get user data
     * 
     * @param type $field Field to retrieve
     * @param type $uid User ID number
     * 
     * @return string Field data
     */
    function get($field, $uid) {
        $query = "SELECT " . $field . " FROM users WHERE id = '" . $uid . "'";
        return $this->config->mysqlQuery($query)[0][$field];
        
    }
    
    /**
     * @title get($field, $uid)
     * @desc Set user data
     * 
     * @param type $field Field name to set
     * @param type $value Field value to enter
     * @param type $uid User ID number
     * 
     * @return string Field data
     */
    function set($field, $value, $uid) {
        $query = "UPDATE users SET '" . $field . "' = '" . $value . "' WHERE id = '" . $uid . "'";
        $this->logger->log('user', $query);
        return $this->config->mysqlQuery($query)[0][$field];
        
    }
    
    
}
