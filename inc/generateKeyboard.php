<?php

/**
 * 
 *
 *
 * @category   CategoryName
 * @package    PackageName
 * @author     Alex Schittko <alex@bracketangles.net>
 * @copyright  2017 Alex Schittko
 * @license    Private
 */
require('config.php');
$k = 0;
$maxHooks = 100; // How many hooks on the board?
$query = "INSERT INTO keyboard (position_id, customer_id) VALUES ";
for ($k = 0; $k <= $maxHooks; $k++) {
    $query .= "(" . $k . ", 0)";
    if ($k == $maxHooks) {
        $query .= ";";
    }
    else {
        $query .= ',';
    }
}
print_r($query);
$config->mysqlQuery($query);