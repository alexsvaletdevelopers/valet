<?php

/**
 * 
 *
 *
 * @category   CategoryName
 * @package    PackageName
 * @author     Alex Schittko <alex@bracketangles.net>
 * @copyright  2017 Alex Schittko
 * @license    Private
 */

/**
 * Description of customer
 *
 * @author alexschittko
 */
class customer {
    /*
     * @desc Create 
     */
    
    function __construct() {
        $this->id = 0;
        $this->name = '';
        $this->phone = '';
        $this->populated = false;
        $this->config = new config();
    }
    
    public function setID($id) {
        $this->id = $id;
        if (!$this->populated) {
            $this->populateByID();
        }
        $this->updateCustomerData('id', $id);
    }
    public function setName($name) {
        $this->name = $name;
        $this->updateCustomerData('name', $name);
    }
    public function setPhone($phone) {
        $this->phone = $phone;
        if (!$this->populated) {
            $this->populateByPhone();
        }
        $this->updateCustomerData('phone', $phone);
    }
    
    private function createCustomer() {
        $result = $this->config->mysqlQuery("INSERT INTO customers SET phone = '" . $this->phone . "', name  = '" . $this->name . "'", false);
        $customer = $this->config->mysqlQuery("SELECT * FROM customers WHERE phone = '".$this->phone."'");
        $this->setID($customer[0]['id']);
        //$this->setName($customer[0]['name']);
        //$this->setPhone($customer[0]['phone']);
        
        return $customer[0];
    }
    
    public function getCustomerData($field) {
        $query = "SELECT " . $field . " FROM customers WHERE id = '" . $this->id ."'";
        $row = $this->config->mysqlQuery($query)[0]; 
        return $row[$field];
        
    }
    
    private function populateByPhone() {
        $result = $this->config->mysqlQuery("SELECT * FROM customers WHERE phone = '" . $this->phone . "'");
        if (!$result) {
            $this->createCustomer();
            
        }
        else {
             $this->id = $result[0]['id'];
             $this->setName($result[0]['name']);
        }
        $this->populated = true;
    }
    
    private function populateByID() {
        $result = $this->config->mysqlQuery("SELECT * FROM customers WHERE id = '" . $this->id . "'");
        if (!$result) {
            $this->createCustomer();
        }
        else {
            $this->phone = $result[0]['phone'];
            $this->setName($result[0]['name']);
        }
        $this->populated = true;
    }
    private function updateCustomerData($field, $value) {
        if ($this->id != 0) {
            $query = "UPDATE customers SET ".$field." = '" . $value . "' WHERE id = '" . $this->id . "'";
            return $this->config->mysqlQuery($query, false);
        }
        else {
            return array();
        }
        
    }
}
