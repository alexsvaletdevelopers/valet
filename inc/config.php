<?php

class config {

    function __construct() {
         $this->config = array(
            'dbaddr' => "127.0.0.1",
            'dbuser' => 'valet',
            'dbpass' => '-3rkvjQY@?Pw#F&X',
            'dbname' => 'valetspot',
            'siteurl' => 'http://valetspot.azurewebsites.net', #NO TRAILING SLASH
            'keyboardurl' => 'keyboard',
            'loginurl' => 'login.php',
            'legalurl' => 'index.php?action=legal',
            'loginexpire' => 'login.php?action=expire',
            'logintimeout' => 99999999999999999,
            
        );
        $this->smsUser = 'alex4108@live.com';
        $this->smsPass = 'geekAY12';
    }
    function getMysqli() {
        return new mysqli($this->config['dbaddr'], $this->config['dbuser'], $this->config['dbpass'], $this->config['dbname']);
    }
    /*
     * @param query
     * @param return Return a data set (bool)
     */
    function mysqlQuery($query = "", $return = true) {
        $mysqli = $this->getMysqli();
        $result = $mysqli->query($query);
        if ($return == false) { // If we know it's an update query, return the boolean, not an array
            return $result;
        }
        if ($result === FALSE) {
            return false;
        }
        else
            return $result->fetch_all(MYSQLI_ASSOC);
    }
    function mysqlEscape($e = "") {
        $mysqli = $this->getMysqli();
        return $mysqli->real_escape_string($e);
    }
    function getConfig($k) {
        return $this->config[$k];
    }
    function closingMessage() {
        $query = "SELECT * FROM settings WHERE `key` = 'closingMessage'";
        return $this->mysqlQuery($query)[0]['value'];
    }
    function checkinMessage($data) {
        // {ticketNumber} / $data['keyNumber']
        $query = "SELECT * FROM settings WHERE `key` = 'checkinMessage'";
        $message = $this->mysqlQuery($query)[0]['value'];
        return str_replace('{ticketNumber}', $data['keyNumber'], $message);
    }
    function checkoutMessage() {
        
        $query = "SELECT * FROM settings WHERE `key` = 'checkoutMessage'";
        return $this->mysqlQuery($query)[0]['value'];
    }
}
$config = new config();