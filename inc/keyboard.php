<?php

/**
 * 
 * @author     Alex Schittko <alex@bracketangles.net>
 * @copyright  2017 Alex Schittko
 * @license    Private
 */

class keyboard {
    function __construct() {
        $this->config = new config();
        $this->logger = new logger();
        $this->sms = new sms();
    }
    function getKeyboard() {
        return $this->config->mysqlQuery("SELECT * FROM keyboard"); /* $keyboard['position_id'] && $keyboard['customer_id']; */  
    }
    function checkin($customer) {
        $keyNumber = $this->getNextKeyNumber();
        $result = $this->config->mysqlQuery("UPDATE keyboard SET customer_id ='" . $customer->id . "' WHERE position_id = '" . $keyNumber . "'", false);
       
        $this->logger->log('checkin', "Check In: Key: " . $keyNumber . " | " . $customer->getCustomerData('phone') . " | " . $customer->getCustomerData('name'));
        $this->sms->sms('checkin', $customer->getCustomerData('phone'), $keyNumber);
        
        if ($result) {
            return $keyNumber;
        }
        else {
            return -1;
        }
        
    }
    
    function doCheckOut($customerNumber, $keyNumber) {
        $customer = new Customer();
        $customer->setPhone($customerNumber);
        $result = $this->config->mysqlQuery("UPDATE keyboard SET customer_id ='0', location = \"NULL\" WHERE position_id = '" . $keyNumber . "'", false);
        //$this->sms->sms('checkout', $customer->getCustomerData('phone'), $keyNumber);
        $this->logger->log('checkout', "Check Out: " . $keyNumber . " | Customer: " . $customer->getCustomerData('name') . " (" . $customer->getCustomerData('id') . ").");         
    }
    /**
     * @desc Get all in use key hook numbers
     * @param type $customerID (optional) Get in use key hooks for selected customer only
     * @return array Key: position ID, value: Customer ID
     */
    function getInUseKeys($customerID = 0) {
        
        $returnArray = array();
        
        if ($customerID == 0) {
            $result = $this->config->mysqlQuery("SELECT * FROM keyboard WHERE customer_id != 0");
        }
        else {
            $result = $this->config->mysqlQuery("SELECT * FROM keyboard WHERE customer_id = '" . $customerID . "'");  
        }
        foreach ($result as $k => $v) {
                $returnArray[$v['position_id']] = $v['customer_id'];
        }
        return $returnArray;
    }
    function getNextKeyNumber() {
        $result = $this->config->mysqlQuery("SELECT * FROM keyboard");
        
        foreach ($result as $k => $v) {
            if ($v['customer_id'] == 0) {
                return $v['position_id'];
            }
        }   
    }
    function getLocation($key) {
        $key = $this->config->mysqlEscape($key);
        $result = $this->config->mysqlQuery("SELECT * FROM keyboard WHERE 'position_id' = '" . $key . "'");
        foreach ($result as $k => $v) {
            return $v['location'];
        }
    }
    function updatePosition($key, $position) {
        $key = $this->config->mysqlEscape($key);
        $position = $this->config->mysqlEscape($position);
        
        $result = $this->config->mysqlQuery("UPDATE keyboard SET 'location' = '" .$position . "' WHERE 'position_id' = '" . $key . "'", false);
        if ($result == False) {
            die("Error @ updatePosition");
        }
    }
    function html() {
        
        $keyboard = $this->getKeyboard();
        $counter = 0;
        for($counter = 0; $counter < count(array_keys($keyboard)); $counter+=2) {
            
            
            $customer = new customer();
            $customer->setID($keyboard[$counter]['customer_id']);
            
            $customer1 = new customer();
            $customer1->setID($keyboard[$counter+1]['customer_id']);
            
            $customer2 = new customer();
            $customer2->setID($keyboard[$counter+2]['customer_id']);
            
            
            //print_r($customer);
            if ($keyboard[$counter]['location'] == "NULL") {
                $keyboard[$counter]['location'] = "";
            }
            if ($keyboard[$counter+1]['location'] == "NULL") {
                $keyboard[$counter+1]['location'] = "";
            }
            // TODO: Convert whole form to AJAX.... how to we differentiate each form from another in JS?
            $html .= "<p align=\"center\"><table><tr><td>" . $keyboard[$counter]['position_id'] . "</td><td>" . $keyboard[$counter+1]['position_id'] . "</td></tr>"
                    . "<tr><td>" . $customer->getCustomerData('name') . "</td>". "<td>" . $customer1->getCustomerData('name') . "</td>"
                    . "<tr><td><input type=\"text\" class=\"input\" name=\"phone\" id=\"phone\" value=\"" . $customer->getCustomerData('name') . "\" onChange=\"updatePhone(this.value, ". $cusomter->getCustomerData('phone') .") \" /></td>". "<td><input type=\"text\" class=\"input\" name=\"phone\" id=\"phone\" value=\"" . $customer1->getCustomerData('phone') . "\" onChange=\"updatePhone(this.value, ". $cusomter1->getCustomerData('phone') .") \" /></td>"
                    . "<tr><td><input type=\"text\" class=\"input\" name=\"location\" id=\"location\" value=\"" . $keyboard[$counter]['location'] . "\" onChange=\"updateLocation(this.value, ". $keyboard[$counter]['position_id'] .") \" /></td><td><input class=\"input\" type=\"text\" name=\"location\" id=\"location\" value=\"" . $keyboard[$counter+1]['location'] . "\" onChange=\"updateLocation(this.value, ". $keyboard[$counter+1]['position_id'] .") \" /></td></tr>"
                    . "<tr><td><a href=\"index.php?action=checkout&phone=" . $customer->getCustomerData('phone') ."&key=" . $keyboard[$counter]['position_id'] . "\">Check Out</a></td>"
                    . "<td><a href=\"index.php?action=checkout&phone=" . $customer1->getCustomerData('phone') ."&key=" . $keyboard[$counter+1]['position_id'] . "\">Check Out</a></td>"
                    . "</tr></table><hr /></p>";
            } 
            $html .= "<br /><br /><br />";
            $html .= '<p align="center"><a href="index.php?action=closeAlert">Close the Stand </a></p>';
            
        return $html;
    }
}

$keyboard = new keyboard();
