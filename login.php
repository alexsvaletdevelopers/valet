<?php
/**
 * Handles sign in actions and presents sign in form
 *
 * @package    Valet
 * @author     Alex Schittko <alex@bracketangles.net>
 * @copyright  2017 Alex Schittko
 * @license    Private
 */
require('inc/config.php');

if (isset($_GET['action'])) {
	if ($_GET['action'] == "md5") {
		print_r(md5('geekAY12'));
		die();
	}
	if ($_GET['action'] == "sign_in") {
		session_start();
		if (isset($_SESSION['user'])) {
			session_unset();
			session_destroy();
			session_start();
		}
                $usr = $config->mysqlEscape($_POST['username']);
                $pas = md5($config->mysqlEscape($_POST['password']));
                
                $row = $config->mysqlQuery("SELECT * FROM users WHERE user = '" . $usr . "' AND pass = '" . $pas . "'");
                $row = $row[0]; // Single row result
                if ($row['access'] > 0) {
		
                        $_SESSION['uid'] = $row['id'];
			$_SESSION['user'] = $row['user'];
                        $_SESSION['access'] = intval($row['access']);
			$_SESSION['expire'] = time()+$config->getConfig('logintimeout');
			//print_r("TIME: " . time());
			//print_r("EXPIRE: " . $_SESSION['expire']);
			$m = "Signed in";
			header("Location: " . $config->getConfig('siteurl'));
		}
		else {
			$m = "Invalid username or password.";
		}
	}
	if ($_GET['action'] == "sign_out" || $_GET['action'] == "expire") {
		session_unset();
		session_destroy();
                switch($_GET['action']) {
                    case "sign_out": $m = "Successfully signed out."; break;
                    case "expire": $m = "Expired after " . $config->getConfig('logintimeout') . " seconds no activity.  Please sign in again."; break;
                }
	}
    }
?>
<html>
<body>
<center>
<?php
	if (isset($m)) {
		print("<font color='red'><b>" . $m . "</b></font>");
	}
?>

<form action="login.php?action=sign_in" method="post">
<table>
<tr>
	<td width="25%"></td>
	<td>Username:</td>
	<td><input type="text" name="username" width="80px" height="15px"/></td>
</tr>
<tr>
	<td width="25%"></td>
	<td>Password:</td>
	<td><input type="password" name="password" /></td>
</tr>
</table>

<input type="Submit" style="font-size: 8vw" value="Sign In" />
</center>
</form>
</body>
</html>
