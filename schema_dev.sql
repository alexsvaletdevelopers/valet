-- Adminer 4.3.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP DATABASE IF EXISTS `valet_dev`;
CREATE DATABASE `valet_dev` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `valet_dev`;

DROP TABLE IF EXISTS `customers`;
CREATE TABLE `customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone` text NOT NULL,
  `name` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `customers` (`id`, `phone`, `name`) VALUES
(0,	'9999999999',	'Unassigned'),
(16,	'0',	'k'),
(17,	'9728548298',	'Alex'),
(18,	'999',	''),
(19,	'12',	''),
(20,	'99',	''),
(21,	'2',	'4'),
(22,	'3',	'q'),
(23,	'9',	''),
(24,	'',	''),
(25,	'2149992810',	''),
(26,	'9728532092',	''),
(27,	'2222',	''),
(28,	'22',	'jackson'),
(29,	'2149998421',	'Loser');

DROP TABLE IF EXISTS `keyboard`;
CREATE TABLE `keyboard` (
  `position_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  PRIMARY KEY (`position_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `keyboard` (`position_id`, `customer_id`) VALUES
(1,	0),
(2,	0),
(3,	0),
(4,	0),
(5,	0),
(6,	0),
(7,	0),
(8,	0),
(9,	0),
(10,	0),
(11,	0),
(12,	0),
(13,	0),
(14,	0),
(15,	0),
(16,	0),
(17,	0),
(18,	0),
(19,	0),
(20,	0),
(21,	0),
(22,	0),
(23,	0),
(24,	0),
(25,	0),
(26,	0),
(27,	0),
(28,	0),
(29,	0),
(30,	0),
(31,	0),
(32,	0),
(33,	0),
(34,	0),
(35,	0),
(36,	0),
(37,	0),
(38,	0),
(39,	0),
(40,	0),
(41,	0),
(42,	0),
(43,	0),
(44,	0),
(45,	0),
(46,	0),
(47,	0),
(48,	0),
(49,	0),
(50,	0),
(51,	0),
(52,	0),
(53,	0),
(54,	0),
(55,	0),
(56,	0),
(57,	0),
(58,	0),
(59,	0),
(60,	0),
(61,	0),
(62,	0),
(63,	0),
(64,	0),
(65,	0),
(66,	0),
(67,	0),
(68,	0),
(69,	0),
(70,	0),
(71,	0),
(72,	0),
(73,	0),
(74,	0),
(75,	0),
(76,	0),
(77,	0),
(78,	0),
(79,	0),
(80,	0),
(81,	0),
(82,	0),
(83,	0),
(84,	0),
(85,	0),
(86,	0),
(87,	0),
(88,	0),
(89,	0),
(90,	0),
(91,	0),
(92,	0),
(93,	0),
(94,	0),
(95,	0),
(96,	0),
(97,	0),
(98,	0),
(99,	0),
(100,	0);

DROP TABLE IF EXISTS `log`;
CREATE TABLE `log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` text NOT NULL,
  `event` text NOT NULL,
  `user` text NOT NULL,
  `timestamp` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `log` (`id`, `key`, `event`, `user`, `timestamp`) VALUES
(1,	'checkin',	'Check In: 9728548298 | Alex',	'1',	NULL),
(2,	'checkin',	'Check In: 9728548298 | Alex',	'1',	'1511799868'),
(3,	'checkin',	'Check In: 9728548298 | Alex',	'1',	'1511799960'),
(4,	'checkin',	'Check In: 9728548298 | Alex',	'1',	'1511800036'),
(5,	'checkin',	'Check In: 9728548298 | Alex',	'1',	'1511800065'),
(6,	'checkin',	'Check In: 9728548298 | Alex',	'1',	'1511800093'),
(7,	'checkin',	'Check In: 9728548298 | Alex',	'1',	'1511800136'),
(8,	'checkin',	'Check In: 9728548298 | Alex',	'1',	'1511800230'),
(9,	'checkin',	'Check Out: 17 | Customer: Alex (17).',	'1',	'1511800247'),
(10,	'checkin',	'Check Out: 17 | Customer: Alex (17).',	'1',	'1511800247'),
(11,	'checkin',	'Check Out: 17 | Customer: Alex (17).',	'1',	'1511800247'),
(12,	'checkin',	'Check Out: 17 | Customer: Alex (17).',	'1',	'1511800247'),
(13,	'checkin',	'Check Out: 17 | Customer: Alex (17).',	'1',	'1511800247'),
(14,	'checkin',	'Check Out: 17 | Customer: Alex (17).',	'1',	'1511800247'),
(15,	'checkin',	'Check Out: 17 | Customer: Alex (17).',	'1',	'1511800247'),
(16,	'checkin',	'Check Out: 17 | Customer: Alex (17).',	'1',	'1511800247'),
(17,	'checkin',	'Check Out: 17 | Customer: Alex (17).',	'1',	'1511800247'),
(18,	'checkin',	'Check Out: 17 | Customer: Alex (17).',	'1',	'1511800247'),
(19,	'checkin',	'Check Out: 17 | Customer: Alex (17).',	'1',	'1511800247'),
(20,	'checkin',	'Check Out: 17 | Customer: Alex (17).',	'1',	'1511800247'),
(21,	'checkin',	'Check Out: 17 | Customer: Alex (17).',	'1',	'1511800247'),
(22,	'checkin',	'Check Out: 17 | Customer: Alex (17).',	'1',	'1511800247'),
(23,	'checkin',	'Check Out: 17 | Customer: Alex (17).',	'1',	'1511800247'),
(24,	'checkin',	'Check In: 9728548298 | Alex',	'1',	'1511800334'),
(25,	'checkin',	'Check Out: 17 | Customer: Alex (17).',	'1',	'1511800347'),
(26,	'checkin',	'Check In: 9728548298 | Alex',	'1',	'1511800388'),
(27,	'checkin',	'Check Out: 17 | Customer: Alex (17).',	'1',	'1511800401'),
(28,	'checkin',	'Check In: 9728548298 | Alex',	'1',	'1511800469'),
(29,	'checkin',	'Check In: 9728548298 | Alex',	'1',	'1511800473'),
(30,	'checkin',	'Check In: 9728548298 | Alex',	'1',	'1511800477'),
(31,	'checkin',	'Check Out: 17 | Customer: Alex (17).',	'1',	'1511800595'),
(32,	'checkin',	'Check Out: 17 | Customer: Alex (17).',	'1',	'1511800596'),
(33,	'checkin',	'Check Out: 17 | Customer: Alex (17).',	'1',	'1511800596'),
(34,	'checkin',	'Check In: 9728548298 | Alex',	'1',	'1511800653'),
(35,	'checkin',	'Check Out: 17 | Customer: Alex (17).',	'1',	'1511800674'),
(36,	'checkin',	'Check Out: 0 | Customer: Unassigned (0).',	'1',	'1511800931'),
(37,	'checkin',	'Check Out: 0 | Customer: Unassigned (0).',	'1',	'1511800932'),
(38,	'checkin',	'Check Out: 0 | Customer: Unassigned (0).',	'1',	'1511800933'),
(39,	'checkin',	'Check Out: 0 | Customer: Unassigned (0).',	'1',	'1511800933'),
(40,	'checkin',	'Check Out: 0 | Customer: Unassigned (0).',	'1',	'1511800934'),
(41,	'checkin',	'Check Out: 0 | Customer: Unassigned (0).',	'1',	'1511800935'),
(42,	'checkin',	'Check Out: 0 | Customer: Unassigned (0).',	'1',	'1511800936'),
(43,	'checkin',	'Check Out: 0 | Customer: Unassigned (0).',	'1',	'1511800937'),
(44,	'checkin',	'Check Out: 0 | Customer: Unassigned (0).',	'1',	'1511800937'),
(45,	'checkin',	'Check In: 9728548298 | Alex',	'1',	'1511800945'),
(46,	'checkin',	'Check Out: 17 | Customer: Alex (17).',	'1',	'1511800954'),
(47,	'checkin',	'Check Out:  | Customer:  (24).',	'1',	'1511801171'),
(48,	'checkin',	'Check Out:  | Customer: Unassigned (0).',	'1',	'1511801188'),
(49,	'checkin',	'Check Out:  | Customer: Unassigned (0).',	'1',	'1511801215'),
(50,	'checkin',	'Check Out: 1 | Customer: Unassigned (0).',	'1',	'1511801229'),
(51,	'checkin',	'Check In: 9728548298 | Alex',	'1',	'1511801236'),
(52,	'checkin',	'Check Out: 1 | Customer: Alex (17).',	'1',	'1511802743');

DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` text NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `settings` (`id`, `key`, `value`) VALUES
(1,	'checkinMessage',	'Welcome to RR Valet!  Your ticket number is: {ticketNumber}.\r\n\r\nUpon the completion of your visit, please reply the word \"car\" to request your vehicle pulled to the entrance upon the completion of your visit.\r\n\r\nLegal: goo.gl/link'),
(2,	'checkoutMessage',	'You have been checked out of RR Valet.'),
(3,	'closingMessage',	'The valet stand will be closing shortly.  Please see the attendant to claim your key.  After 15 minutes, all unclaimed keys will be left with venue staff.');

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(45) NOT NULL,
  `pass` varchar(45) NOT NULL,
  `access` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `phone` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `users` (`id`, `user`, `pass`, `access`, `email`, `phone`) VALUES
(1,	'alex',	'51053c224b86990fbb782a005b17d2e7',	'10',	'alex4108@live.com',	'9728548298');

-- 2017-11-27 17:40:33
