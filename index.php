<?php
define('HASH', 'asoidj0jasclpapo12e90px12');
if (isset($_GET['action']) && $_GET['action'] == "legal") {
    index::displayLegal();
    die();
}
require_once('inc/config.php');
require_once('sessionCheck.php');
require_once('inc/logger.php');
require_once('inc/sms.php');
require_once('inc/customer.php');
require_once('inc/keyboard.php');
require_once('inc/checkin.php');
require_once('inc/user.php');
require_once('inc/header.php');

class index {
    function __construct() {
        $this->checkin = new checkin();
        $this->keyboard = new keyboard();
        $this->logger = new Logger();
        $this->config = new config();
        $this->sms = new sms();
    }
    function displayCheckin() {
        $customer = new Customer();
        $customer->setPhone($_POST['phone']);
        $customer->setName($_POST['name']);
        $keyboard = new Keyboard();
        
        $keyNumber = $keyboard->checkin($customer);
        
        print "Checked in! Hook #: " . $keyNumber . "<br />";
        print "<input type=\"text\" class=\"input\" name=\"location\" id=\"location\" value=\"" . $keyboard->getLocation($keyNumber) . "\" onChange=\"updateLocation(this.value, ". $keyNumber .") \" />"; // Location field
    }
    
    function displayCheckout($phoneNumber, $keyNumber) {
        $this->keyboard->doCheckout($phoneNumber, $keyNumber);
        print "Checked Out!";
    }
    
    function displayLog() {
        print '<table>
            <tr><td>Tag</td><td>Event</td><td>User</td><td>Timestamp</td></tr>';
        $log = $this->logger->displayAll();
        //print_r($log);
        foreach ($log as $k => $v) {
            print_r("<tr><td>". $v['key'] ."</td><td>". $v['event'] ."</td><td>". $v['user'] ."</td><td>". gmdate('Y-m-d\TH:i:s\Z', $v['timestamp']) ."</td></tr>");
        }
    }
    
    function incomingSMS() {
        // Send SMS to Valet
        if (payload == "car") { 
            echo 'Your vehicle has been requested.  Please continue to the exit to meet your car.';
        }
        elseif (payload == "legal") {
           displayLegal();
        }
    }
    
    public static function displayLegal() {
        echo "This  Parking  Agreement  ('Agreement')  is  between  R&R VALET INC. ('R&R VALET, R&R')     and ____________________  ('Client')  for  valet  parking  service.\n
In  consideration  of  the  parking  services provided  by  R&R Valet,  for  the  vehicle  listed  below. Client  agrees  to  be  bound  by  all  the 
following terms
and conditions by signing this Agreement.\n
Terms and Conditions:\n
1.
Valet Parking.\n
Valet parking is offered by the venue for the convenience of the Client and 
only for so long as Client is a customer or employee of the venue.\n
\n
2.
Payments by Client.\n
All services are operated with a price sign displayed at the enterance.  Failure to pay the posted price may result in criminal proceedings.\n
\n
3.
No  Duty  to  Patrol.\n
Client  acknowledges  that  venue and parking attendant have no  duty  to  patrol  or  post 
security at parking lot.\n
\n
4.
Items Left in Vehicle.\n
R&R Valet and venue shall not be responsible for damage or loss to possessions 
or items left in Client’s vehicle at any time.\n
\n
5.
Damage  to  Vehicle.\n
R&R Valet and venue  shall  not  be  responsible  for  damage  to  Client’s  vehicle, 
whether  or  not  such  damage  is  caused  by  other  vehicle(s)  or  person(s)  in  the  parking  lot  and 
surrounding area.\n
\n
6.
Parking  Lot  Attendants\n
R&R valet and vanue  provide
s  parking  lot  attendants,  also  referred  to  as 
valets, to park vehicles. Use of such attendants by The Pawington to park or drive Client’s vehicle 
shall be at the sole risk of Client. \n
\n
7.
Acceptance of Risk.\n
Client agrees to assume responsibility for the risk of property damage while Client’s vehicle is parked on venue’s property and/or parked by R&R’s or venue\'s valets.\n
\n
8.
Release and Indemnity.\n
CLIENT AGREES TO RELEASE, WAIVE, DISCHARGE AND CONVENANT NOT TO SUE R&R VALET, VENUE, ITS OWNERS, DIRECTORS, OFFICERS, EMPLOYEES,  AND  AGENTS.    CLIENT  AGREES  TO  INDEMNIFY  AND  SAVE  AND HOLD    HARMLESS    R&R VALET, THE VENUE,    ITS    OWNERS,    DIRECTORS,OFFICERS, EMPLOYEES,  AND  AGENTS  FROM  ANY  LOSS,  LIABILITY,  THEFT,  DAMAGE,
OR 
COST  THAT  MAY  INCUR  DUE  TO  THE  VALET  PARKING  OF  CLIENT’S  VEHICLE UPON THE VENUE'S PROPERTY, WHETHER CAUSED BY NEGLIGENCE OF THE R&R VALET OR OTHERWISE."; 
    }
  
    
function sendCloseAlert() {
    $message = $this->config->checkoutMessage();
    $keys = $this->keyboard->getInUseKeys();
    foreach($keys as $key => $cid) {
        $customer = new Customer();
        $customer->setID($cid);
        
        $this->sms->sms('closing', $customer->getCustomerData('phone'), $key);
        $this->logger->log('closing', "SMS: " . $key . " | Customer: " . $customer->getCustomerData('name') . " (" . $customer->getCustomerData('id') . ").");         
    
    }
    return "All clients SMSed!";
}
    function router($value) {
        switch($value) {
            case "keyboard": print($this->keyboard->html()); break;
            case "checkin": print($this->checkin->generateForm()); break;
            case "doCheckin": $this->displayCheckin(); break;
            case "checkout": print($this->displayCheckout($_GET['phone'], $_GET['key'])  ); break;
            case "viewLog": print($this->displayLog()); break;
            case "inbound": print($this->incomingSMS()); break;
            case "legal": print($this->displayLegal()); break;
            case "closeAlert": print($this->sendCloseAlert()); break;
        }

    }
}

$index = new index();
// content switcher
if (isset($_GET['action'])) {
        $index->router($_GET['action']);
}
else {
    $index->router("keyboard");
}
                



require_once('inc/footer.php');