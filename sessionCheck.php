<?php
if ( HASH != "asoidj0jasclpapo12e90px12") {
    die();
}
session_start();

//print_r("EXPIRE TIME: " . $_SESSION['expire'] . "<br />");
//print_r("CURRENT TIME: " . time() . "<br />");
if (time() <  $_SESSION['expire']) {
	//print("Session Valid.  New Session Expire Time: " . (time()+120) . "<br />");
}
else {
	//print("Session Expired.");
}
//die();


if ($_SESSION['user'] == null) { // No Session 
	header("Location: " . $config->getConfig('siteurl') ."/" . $config->getConfig('loginurl'));
	die();
}
if ($_SESSION['expire'] < time()) { // Expired session
	header("Location: " . $config->getConfig('siteurl') ."/" . $config->getConfig('loginexpire'));
	die();
}

if ($_SESSION['expire'] > time()) { // Valid session
	$_SESSION['expire'] = (time()+$config->getConfig('logintimeout'));
}
//print_r("Session expires at " . date("h:i:s", $_SESSION['expire']));

