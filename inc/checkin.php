<?php

/**
 *
 * @category   CategoryName
 * @package    PackageName
 * @author     Alex Schittko <alex@bracketangles.net>
 * @copyright  2017 Alex Schittko
 * @license    Private
 */

class checkin {
    
    function __construct() {
        $this->config = new config();
        $this->logger = new logger();
        $this->sms = new sms();
    }
    
    
    /*
     * @title doCheckin
     * 
     * @desc Checks in the customer on the next available key hook
     * @param string $customerNumber customer Phone number
     * @param string $customerName customer name
     * 
     * @return int Returns the Key Number assigned to the checkin
         */
    /*function doCheckin($customerNumber, $customerName = "") {
        $customer = new Customer();
       
        $customer->setName($customerName);
        $customer->setPhone($customerNumber);
        $keyboard = new Keyboard();
        $keyNumber = $keyboard->checkin($customer);
        
        print_r($this->logger->log('checkin', "Check In: " . $customerNumber . " | " . $customerName));
        $this->sms->sms('checkin', $customerNumber, $keyNumber);
        return $keyNumber;
        
    }*/
    
    function doCheckOut($customerNumber) {
        $customer = new Customer();
        $customer->setPhone($customerNumber);
        $keyboard = new Keyboard();
        $customerKeys = $keyboard->getInUseKeys($customer->getCustomerData('id'));
        foreach($customerKeys as $k => $v) {
            $keyboard->checkout($k, $customer);
            $this->logger->log('checkout', "Check Out: " . $v . " | Customer: " . $customer->getCustomerData('name') . " (" . $customer->getCustomerData('id') . ").");
        }           
    }
    
    /*
     * @title generateForm
     * 
     * @desc Generates the HTML checkin form
     * @param null
     * 
     * @return string HTML checkin form
     */
    function generateForm() { // Checkin form
         
        
        $html = "<form action=\"index.php?action=doCheckin\" method=\"POST\"><p align=\"center\"><table width=\"80%\" border=\"1\">";
        $html .= "<tr><td>Phone:</td><td><p align=\"center\"><input class=\"input\" id=\"phone\" width=\"100%\" type=\"text\" name=\"phone\" onChange=\"showUser(this.value)\" /></p></td></tr>"
                . "<tr><td>Name:</td><td><p align=\"center\"><input class=\"input\" id=\"name\" width=\"100%\"  type=\"text\" name=\"name\" /></p></td></tr>";
     
        $html .= "</table><input type=\"submit\" class=\"input\"  value=\"Check In\"></input></form><br /></p>";
        return $html;
        
    }
}
